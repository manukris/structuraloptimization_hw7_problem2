function lv = locvec( nodes, ndf )
%LOCVEC Locator vector, usage: lv = locvec( nodes, ndf )
%
% Locator Vector
%
%--Input
%
%  nodes... Nodal connectivity (row vector)
%  ndf..... Number of nodal degrees of freedom
%
%--Ouput
%
%  lv...... Locator vector from element to system DoFs
%
npe = size(nodes,2); % nodes per element
lv = (repmat(nodes,ndf,1)-1)*ndf + repmat((1:ndf)',1,npe);
lv = lv(:);
