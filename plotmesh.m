function [xc,yc] = plotmesh( mesh, xy )
%PLOTMESH Plot element mesh
[nelm,npe]=size(mesh);
set(gca,'DataAspectRatio',[1 1 1])
for e=1:size(mesh,1)
	line( xy(mesh(e,[1:npe 1]),1), xy(mesh(e,[1:npe 1]),2) )
end
% Label the elements
elem = cell(nelm,1);
for e=1:nelm
	elem{e}=int2str(e);
end
xc = mean( reshape( xy(mesh,1), size(mesh)), 2 );
yc = mean( reshape( xy(mesh,2), size(mesh)), 2 );
text(xc, yc, elem, 'HorizontalAlignment', 'center')
title('Element numbers')
