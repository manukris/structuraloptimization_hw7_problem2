function mass_matrix = truss_mass( rho, A, L, ndf )
%
%--Input
%
%  rho..... Mass density
%  A....... Truss cross-sectional area
%  L....... Truss length
%  ndf..... Number of Nodal Degrees of Freedom
%
%--Output
%  mass_matrix... Truss element lumped (diagonal) mass matrix

if isempty(L) || isnan(L) || L<=0
    error('truss_mass:nonpositiveL','Element length, L, must be positive')
elseif isempty(A) || isnan(A) || A<=0
    error('truss_mass:nonpositiveA','Element cross-sectional area, A, must be positive')
elseif isempty(rho) || isnan(rho) || rho<=0
    error('truss_mass:nonpositiveDensity','Mass density, rho, must be positive')
elseif isempty(ndf) || isnan(ndf) || ndf<=0
    error('truss_mass:nonpositiveNDF','Nodal degrees of freedom, ndf, must be positive integer')
end
npe = 2; % Nodes per element
edf = npe*ndf; % Element degrees of freedom
mass_matrix = spdiags( rho*A*L/npe*ones(edf,1), 0, edf, edf );