function plotnodes( xy )

%PLOTNODES Plot nodes
for n=1:size(xy,1), node{n}=int2str(n); end
plot(xy(:,1), xy(:,2), 'rx')
text(xy(:,1), xy(:,2), node, 'Color', 'r')
title('Node Numbers')
set(gca,'DataAspectRatio',[1 1 1])
