clear all
clc
close all

%% Setting up the framework - Question 2 Venkaiya 10 bar problem
TenBar
FEM.Area=5*ones(10,1);
disp('Initial Areas for 10-bar = 5.0')
g = 386.4;
FEM.Material.density = 0.1 / g;
FEM=FEAtrussProc(FEM);

%% Vibration Frequency Analysis
FEAtrussVibe

FEAtrussVibeGrad2


%%  Validation step10 bar problem using complex step
disp('Validation of 10 bar using complex step results');
clear all;close all
loc_der=1;
TenBar
FEM.Area=5*ones(10,1);
FEM.Area(loc_der)=FEM.Area(loc_der)+i*eps;
% disp('Initial Areas for 10-bar = 5.0')
g = 386.4;
FEM.Material.density = 0.1 / g;
FEM=FEAtrussProc(FEM);

FEAtrussVibe;
der_lmda=imag(eigval(1:2))/eps;
disp('Derivative of the first two eigenvalue with respect to area of elem 1 by complex step'),disp(der_lmda');

