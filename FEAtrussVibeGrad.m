% Mass normalizing the eigenvectors

alpha=eigvec'*M(u,u)*eigvec;
[row_alpha,col_alpha]=size(alpha);

for ii=1:row_alpha
    eigvec(:,ii)=eigvec(:,ii)/sqrt(alpha(ii,ii));
end
phi=eigvec;
disp('Check mass normalization - o/p should be identity matrix'), disp(eigvec'*M(u,u)*eigvec)
% eigvec'*M(u,u)*eigvec

%%

lmda_dash=zeros(nelm,2);
for j=1:nelm
    j
M_dash=zeros(FEM.N,FEM.N);
K_dash=zeros(FEM.N,FEM.N);
for e=1:nelm
   if j==e
       dum_ar=1;
       m = truss_mass( FEM.Material.density, dum_ar, FEM.L(e), ndf )
       k = truss( FEM.xyz(FEM.mesh(e,:),:), dum_ar, FEM.Material.E )
       p = locvec( FEM.mesh(e,:), ndf )
       M_dash(p,p) = M_dash(p,p) + m; %  M = assemble( m, M, FEM.mesh(e,:) );
       K_dash(p,p) = K_dash(p,p) + k;
%    else
%        dum_ar=FEM.Area(e);
   end
%    m = truss_mass( FEM.Material.density, dum_ar, FEM.L(e), ndf );
%    k = truss( FEM.xyz(FEM.mesh(e,:),:), dum_ar, FEM.Material.E )
%    p = locvec( FEM.mesh(e,:), ndf )
%    M_dash(p,p) = M_dash(p,p) + m; %  M = assemble( m, M, FEM.mesh(e,:) );
%    K_dash(p,p) = K_dash(p,p) + k;
% end
end
lmbda_dash(j,1)=phi(:,1)'*(K_dash(u,u)-eigval(1)*M_dash(u,u))*phi(:,1);
lmbda_dash(j,2)=phi(:,2)'*(K_dash(u,u)-eigval(2)*M_dash(u,u))*phi(:,2);
clear M_dash K_dash
end

% end
lmbda_dash