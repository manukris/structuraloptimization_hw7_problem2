function [k,T,L]=truss( xyz, A, E )
% usage: [k,T,L]=truss( xyz, A, E )
%
% Truss stiffness matrix generation 
% for axial (1-D), planar (2-D), and space (3-D) truss elements

[npe,ndf]=size(xyz);

x = xyz(:,1); dx = diff(x); dy = 0; dz = 0;
if ndf > 1, y = xyz(:,2); dy = diff(y); end
if ndf > 2, z = xyz(:,3); dz = diff(z); end

L = sqrt( dx^2 + dy^2 + dz^2 ); % Truss element length

k = A*E/L*[ 1 -1
           -1  1]; % Element stiffness matrix in local coordinate system

% Transformation matrix for 1-D, 2-D (plane), and 3-D (space) trusses
switch ndf
case 1
   if nargout>1, T=eye(npe,npe); end
   return
case 2
   c = dx / L;
   s = dy / L;
   T = [c s 0 0
        0 0 c s];
case 3
   dc = [dx dy dz] / L;
   T  = [dc    0 0 0
         0 0 0 dc   ];
end
if isreal(T) % For aesthetic reasons, zero out nearly zero terms
   nearzero = find(abs(T(:))<100*eps);
   if any(nearzero), T(nearzero)=0; end
end

% Truss element stiffness in global coordinate system
k  = T.'*k*T;