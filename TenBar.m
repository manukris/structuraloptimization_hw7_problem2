%% TenBar is a script to drive solution of a 10-bar plane truss
%  taken from "Elements of Structural Optimization" Chapter 5
%  by Raphael Haftka and Zafer Gurdal
%
%  Written by: Robert A. Canfield
%              Air Force Institute of Technology
%              AFIT/ENY
%              2950 Hobson Way, Bldg. 640
%              WPAFB, OH 45433-7765
%
% Created: 28 Mar 2006
% Revised: 28 Mar 2006
%
%--Global structure variables set in this script
%
%  FEM... Finite Element Model structure with following fields
%         mesh..... Nodal connectivity matrix for the mesh, one row per element
%         xyz...... Nodal x, y and z coordinates, one row for each node
%         Area..... Cross-Sectional Area for each element (vector)
%         Material. Structure of material properties
%                   E......... Young's Modulus for all elements (scalar)
%                   density... mass density
%                   tension... Allowable axial stress in tension
%                   compress.. Allowable axial stress in compression
%         BC....... Boundary Conditions  Structure with fields: node, direction, magnitude
%         Load..... External Point Loads Structure with fields: node, direction, magnitude
%                   Each field is a cell array indexed to each load case
%         K........ Global Stiffness matrix
%         D........ Global nodal displacement vector
%
% clear
% clear  global
global FEM
%
%--External references
%
%  FEAtruss....... Finite Element Analysis solver for trusses
%  FEAtrussPost... Script to post-process FEA results
%
%--BEGIN
%
%  Save output to a log file
%
format compact
% diary TenBar
%
%% Nodal coordinates
FEM.xyz=[
         720.0,  360.0
         720.0,    0.0
         360.0,  360.0
         360.0,    0.0
           0.0,  360.0
           0.0,    0.0
        ];
figure(1), hold on
plotnodes(FEM.xyz)

%% Define nodal connectivity for mesh
FEM.mesh=[
          3,      5
          1,      3
          4,      6
          2,      4
          3,      4
          1,      2
          4,      5
          3,      6
          2,      3
          1,      4
];
plotmesh( FEM.mesh, FEM.xyz ); hold off
title('Element & Node numbers')

%% Material properties and cross-sectional areas
FEM.Material.E = 10e6;
FEM.Material.density = 0.1;
FEM.Material.tension = 25e3;
FEM.Material.compress = -25e3;
FEM.Area = 5.0*ones(size(FEM.mesh,1),1);

%% Boundary conditions and Loads
FEM.BC.node = [5 6 5 6];
FEM.BC.direction = [1 1 2 2];
clear FEM.Load
FEM.Load.node = [2 4];
FEM.Load.direction = [2 2];
FEM.Load.magnitude = -100e3*[1 1];

%% Call script to conduct finite element analysis

FEAtruss

%% Post-processor

% FEAtrussPost

%  Turn off log file
%
%diary off 