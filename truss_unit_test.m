%% truss_unit_test script to to test truss_stress function
%
% Written by: Robert A. Canfield
%
% Created: 13 Feb 2013
%
%--Data structure variables set in this script
%
%  FEM... Finite Element Model structure with following fields
%         mesh..... Nodal connectivity matrix for the mesh, one row per element
%         xyz...... Nodal x, y and z coordinates, one row for each node
%         Area..... Cross-Sectional Area for each element (vector)
%         Material. Structure of material properties
%                   E......... Young's Modulus for all elements (scalar)
%
%--External references
%
%  truss.......... Finite Element Analysis solver for trusses
%  truss_stress... Script to post-process FEA results
%
%--BEGIN
%
clear; clc

%% Fixed parameters 
%
FEM.Material.E   = 10;
FEM.Area = 1;
%
%% 2-D unit tests
%
% Nodal coordinates and nodal connectivity for each test
%
FEM.xyz=[0 0; 1 0; 0 1; 1 1];
FEM.mesh=[
     1     2 % horizontal
     1     3 % vertical
     1     4 % diagonal
     4     1 % reverse direction diagonal
];
nelm = size(FEM.mesh,1);
%
%  FOR each element generate stiffness.
%
for e=1:nelm
   xy = FEM.xyz(FEM.mesh(e,:),:)
   k = truss( xy, FEM.Area, FEM.Material.E ) %#ok<*NOPTS>
end

%% Unit test for plane truss element stress
% Recover element stresses for prescribed displacements
D = [1 0 2 0; 0 1 0 2; 0 0 1 1; 2 2 1 1]';
for e=1:nelm
   d = D(:,e)
   axial_stress = truss_stress( d, FEM.xyz(FEM.mesh(e,:),:), FEM.Material.E )
end


%% 3-D unit tests
%
% Nodal coordinates and nodal connectivity for each test
%
FEM.xyz=[0 0 0; 1 0 0; 0 1 0; 1 1 1];
FEM.mesh=[
     1     2 % horizontal
     1     3 % vertical
     1     4 % diagonal
     4     1 % reverse direction diagonal
];
nelm = size(FEM.mesh,1);
%
%  FOR each element generate stiffness.
%
for e=1:nelm
   xyz = FEM.xyz(FEM.mesh(e,:),:)
   k = truss( xyz, FEM.Area, FEM.Material.E ) %#ok<*NOPTS>
end

%% Unit test for space truss element stress
% Recover element stresses for prescribed displacements
D = [1 0 0 2 0 0; 0 1 0 0 2 0; 0 0 0 1 1 1; 1 1 1 2 2 2]';
for e=1:nelm
   d = D(:,e)
   axial_stress = truss_stress( d, FEM.xyz(FEM.mesh(e,:),:), FEM.Material.E )
end