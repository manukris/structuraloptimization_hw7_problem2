function [stress,L,strain,force,energy,Pcr,T] = truss_stress( D, xyz, E, A, I )
%  TRUSS_STRESS calculates normal axial stress/strain/force for a truss element.
%
%--INPUT
%
%  D........ Truss element's nodal displacement matrix for global degrees of freedom
%  xyz...... List of coordinates for two nodes in the element
%  E........ Elastic Modulus for this element
%  A........ Cross-sectional area of truss element
%  I........ Cross-sectional area moment of inertia
% 
%--OUTPUT
%
%  stress... Truss element normal stress
%  strain... Truss element normal strain
%  force.... Truss element internal member axial force
%  energy... Element strain energy
%  L........ Length of the truss element
%  Pcr...... Critical value of Euler column buckling load
%  T........ Transformation matrix for global to local coordinates
%
%  Written by: Robert A. Canfield
%     Created: 25 Jun 05
%     Revised: 15 Mar 06
%              15 Feb 13 (to return Pcr)
%               4 Apr 13 (to return strain energy)

if nargin<4, A=1; end
%
%--Local variables
%
%  k........ Truss element stiffness matrix in global coordinates
%  d........ Axial nodal displacements in the local element coordinate system

[k,T,L] = truss( xyz, A, E );

% Stress/strain calculated two different ways for illustrative purposes
d = T * D;
if nargout>2
   r = k * D;            % element forces in global coordinates
   rlocal = T*r;         % element forces in local  coordinates
   force  = rlocal(2,:); % Right end (2nd node) force is positive for tension
   stress = force  / A;
   strain = stress / E;
   energy = sum(rlocal.*d)/2;
else
   strain = diff(d) / L; % strain = B*d; where B = [-1 1]/L;
   stress = E * strain;  % stress = E*B*d;
end

% Critical Euler column buckling load
if nargin>4 && nargout>5
   Pcr = E*I*(pi/L)^2;
end