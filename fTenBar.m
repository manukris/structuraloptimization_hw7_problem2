function [f,g]=fTenBar(x)
% Here f is the weight, we need to minimize the weight criteria
% g is the upperlimit on the fundemental frequency, lower bound is
% specified for that 

TenBar
rho=FEM.Material.density;
[nelm,npe] = size( FEM.mesh );
% f - weight;
f=0;
for e=1:nelm
   [~,FEM.L(e)] = truss( FEM.xyz(FEM.mesh(e,:),:), FEM.Area(e), FEM.Material.E );
   f=f+FEM.L(e)*rho*FEM.Area(e);
     
end





end
