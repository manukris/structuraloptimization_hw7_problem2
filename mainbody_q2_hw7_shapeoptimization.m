clear all
clc
close all
set(0,'defaultaxesfontsize',14,'defaultlinemarkersize',8,...
   'DefaultLineLineWidth',2,'DefaultTextInterpreter','latex','defaultfigurecolor','white')
x    = [ 1; 1];
dxlb = -0.05*ones(1,2);
dxub = 0.05*ones(1,2);

%%
TolX   = 0.01;
TolCon = 1e-3;
%% Loop until converged

converged=false;
disp('   Objective       norm(dx)        max(g)')
options.Display='off';
flg=1;
r1=0.10;r2=0.75;
c1=0.5;c2=2;
obj(1)=10^6;
plt_g(1)=1000;
while ~converged
    flg=flg+1;
    disp(sprintf('Iteration no %d',flg-1));
    [f,g]=fTenBar(x);
    [gradf]=diff_beam_FEM(x);
    c = gradf';
    A = [];
%     b = -g;
    [dx,fval] = linprog(c,[],[],[],[],dxlb,dxub,[],options);
    obj(flg)=beam_FEM(x+dx);
    
    if obj(flg)<obj(flg-1)
    x  = x + dx;
    end
    
    
    ftilde=f+gradf'*dx;
    r(flg)=(f-beam_FEM(x+dx))/(f-ftilde);
    
    if r(flg)<r1
      rad_mov(flg)=c1*norm(dx,inf);
    elseif r(flg)>r2
      rad_mov(flg)=c2*norm(dx,inf);    
    else
      rad_mov(flg)=norm(dx,inf); 
    end
    dxlb=-rad_mov(flg)*ones(2,1);
    dxub=rad_mov(flg)*ones(2,1);
    converged = max(abs(dx)) <= TolX;
    fprintf('%12.3f   %12.4f   %12.4f\n',fval,norm(dx,inf),max(dx))
    
end
    
    
    
    
    
    
    


%%
disp('Final design variables')
disp(x)
disp('Final objective function')
disp(beam_FEM(x))




%% Plotting
figure,plot(1:flg-1,[obj(2:end);r(2:end);rad_mov(2:end)],'linewidth',2);
legend('Objective','Trust ratio','Trust radius');
xlabel('Iteration number');
ylabel('Obj/Trust Ratio/Trust Radius');
grid on
box on

