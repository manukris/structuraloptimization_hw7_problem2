% Mass normalizing the eigenvectors

alpha=eigvec'*M(u,u)*eigvec;
[row_alpha,col_alpha]=size(alpha);

for ii=1:row_alpha
    eigvec(:,ii)=eigvec(:,ii)/sqrt(alpha(ii,ii));
end
phi=eigvec;
disp('Check mass normalization - o/p should be identity matrix'), disp(eigvec'*M(u,u)*eigvec)
% eigvec'*M(u,u)*eigvec

%%

lmda_dash=zeros(nelm,2);
for j=1:nelm
       e=j;
       dum_ar=1;
       m = truss_mass( FEM.Material.density, dum_ar, FEM.L(e), ndf );
       k = truss( FEM.xyz(FEM.mesh(e,:),:), dum_ar, FEM.Material.E );
       p = locvec( FEM.mesh(e,:), ndf );
       [tf,idx] = ismember(p,u);
       loc_extract=idx(idx>0);
       loc_extract2=find(idx>0);
       lmbda_dash(j,1)=phi(loc_extract,1)'*(k(loc_extract2,loc_extract2)-eigval(1)*m(loc_extract2,loc_extract2))*phi(loc_extract,1);
       lmbda_dash(j,2)=phi(loc_extract,2)'*(k(loc_extract2,loc_extract2)-eigval(2)*m(loc_extract2,loc_extract2))*phi(loc_extract,2);

end
disp('Derivatives of the radial frequency squared for all the elements'),disp(lmbda_dash);
