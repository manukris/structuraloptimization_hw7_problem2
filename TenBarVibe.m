%% TenbarVibe.m
% Free Vibration Eigenvalue gradients for 10-bar truss
clear all
clf

TenBar

%% Initial Areas
FEM.Area=5*ones(10,1);
disp('Initial Areas for 10-bar = 5.0')
g = 386.4;
FEM.Material.density = 0.1 / g;
FEM=FEAtrussProc(FEM);

%% Vibration Frequency Analysis
FEAtrussVibe

FEAtrussVibeGrad2