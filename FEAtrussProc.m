function FEM = FEAtrussProc( FEM )
% FEAtrussProc - FEAtruss Processor
%
%% PROCESSOR
%
%  FOR each element generate and assemble stiffness.
[ngrid,ndf]=size(FEM.xyz);
[nelm,npe] =size(FEM.mesh);
K = Kinit( ngrid, ndf, FEM.mesh );
for e=1:nelm
   k = truss( FEM.xyz(FEM.mesh(e,:),:), FEM.Area(e), FEM.Material.E );
   p = locvec( FEM.mesh(e,:), ndf );
   K(p,p) = K(p,p) + k; % K = assemble( k, K, FEM.mesh(e,:) );
end
%
% Specify Boundary Conditions.
%
D = zeros(size(FEM.R)); % Homogenous
nlc = size(FEM.R,2);
%
% Solve simultaneous linear algebraic equations for primary & secondary unknowns.
%
u = FEM.u;
c = FEM.c;
if isreal(K)
   % Solve using decomposed K and forward/back substitution
   FEM.KL = chol( K(u,u) )';                 % Cholesky decomposition
   D(u,:) = FEM.KL' \ ( FEM.KL\FEM.R(u,:) ); % Forward and Back Substition
else
   D(u,:) = K(u,u) \ FEM.R(u,:);
end
%
%% POST-PROCESSOR
%
% Store results as global variables in FEM structure
FEM.R(c,:) = K(c,u)*D(u,:);
FEM.D = D;
FEM.K = K;
D = reshape(D,ndf,ngrid,nlc);
FEM.stress=zeros(nelm,nlc);
FEM.L=size(FEM.Area);
% Recover element stresses
for e=1:nelm
   d = reshape( D(:,FEM.mesh(e,:),:), ndf*npe, nlc );
   [FEM.stress(e,:),FEM.L(e)] = truss_stress( d, FEM.xyz(FEM.mesh(e,:),:), FEM.Material.E );
end