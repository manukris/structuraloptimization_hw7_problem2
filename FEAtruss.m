%% FEAtruss -  script for finite element analysis for trusses 
%
%  Written by: Robert A. Canfield
%  Created:    26 Mar 2006
%  Modified:   13 Feb 2013
%
%--Workspace input (structure) variables
%
%  FEM... Finite Element Model structure with following fields
%         mesh..... Nodal connectivity matrix for the mesh, one row per element
%         xyz...... Nodal x, y and z coordinates, one row for each node
%         Area..... Cross-Sectional Area for each element (vector)
%         Material. Structure of material properties
%                   E......... Young's Modulus for all elements (scalar)
%                   density... mass density
%                   tension... Allowable axial stress in tension
%                   compress.. Allowable axial stress in compression
%         BC....... Boundary Conditions  Structure with fields: node, direction, magnitude
%         Load..... External Point Loads Structure with fields: node, direction, magnitude
%                   Each field is a cell array indexed to each load case
%
%  Output appended as fields to FEM structure
%
%         N........ Number of system degrees of freedom
%         c........ Constrained degrees of freedom
%         u........ Unconstrained (Unknown) degrees of freedom
%         K........ Global Stiffness matrix
%         D........ Global nodal displacement vector
%         R........ Global FEM.Load vector of applied and external reaction loads
%         L........ Length of each truss element
%         stress... Element axial stresses for each load condition
%
%--Local workspace variables
%
%  npe...... Nodes Per Element
%  nelm..... Number of Elements
%  ndf...... Nodal Degrees of Freedom per node
%  ngrid.... Number of grid points (nodes)
%  ldf...... Load degrees of freedom
%  nlc...... Number of Load Cases
%
%--External Function references
%
%  locvec......... Locator vector to assemble current element
%  rhsload........ Assemble Right-hand side load vector
%  Kinit.......... Initialize global stiffness matrix (possibly sparse)
%  truss.......... Generate truss element stiffness matrix and length
%  truss_stress... Calculate normal axial stress/strain/force
%

%--Modifications
%  13 Feb 2013
%  11 Oct 2015 - use locvec instead of assemble
%
%--BEGIN
%
%% PRE-PROCESSOR
%
[nelm,npe] = size( FEM.mesh );
[ngrid,ndf] = size( FEM.xyz );

% Error check size of inputs and initialize stiffness.
if max(FEM.mesh(:)) ~= ngrid, error('Some node # > total number of nodes'), end
[K,FEM.N] = Kinit( ngrid, ndf, FEM.mesh );

% Determine Boundary Condition degrees of freedom.
if ~isfield(FEM.BC,'magnitude'), FEM.BC.magnitude=0; end
c = ((FEM.BC.node-1).*ndf + FEM.BC.direction);
u = setdiff( 1:FEM.N, c );

%
%% PROCESSOR
%
%  FOR each element generate and assemble stiffness.
for e=1:nelm
   k = truss( FEM.xyz(FEM.mesh(e,:),:), FEM.Area(e), FEM.Material.E );
   p = locvec( FEM.mesh(e,:), ndf );
   K(p,p) = K(p,p) + k;
   
end
%
%  Apply external loads.
%
R = rhsload( FEM.Load, ndf, FEM.N );
%
% Specify Boundary Conditions.
%
D = zeros(size(R)); % Homogenous
nlc = size(R,2);
% if any(FEM.BC.magnitude)
%    for n=1:nlc
%       D(c,n) = FEM.BC.magnitude; % Non-homogeneous (enforced displacements)
%    end
% end
%
% Solve simultaneous linear algebraic equations for primary & secondary unknowns.
%
D(u,:) = K(u,u)\R(u,:);
R(c,:) = K(c,u)*D(u,:);
%
%% POST-PROCESSOR
%
% Store results as global variables in FEM structure
FEM.D = D;
FEM.R = R;
FEM.c = c;
FEM.u = u;
D = reshape(D,ndf,ngrid,nlc);
FEM.stress=zeros(nelm,nlc);
% Recover element stresses
for e=1:nelm
   d = reshape( D(:,FEM.mesh(e,:),:), ndf*npe, nlc );
   FEM.stress(e,:) = truss_stress( d, FEM.xyz(FEM.mesh(e,:),:), FEM.Material.E );
end