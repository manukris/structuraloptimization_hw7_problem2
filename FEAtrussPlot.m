function FEAtrussPlot( xyz, mesh )
% FEAtrussPlot  Plot nodes and elements of 3D truss for FEAtruss.m
[ngrid,ndf]=size(xyz);
[nelm, npe]=size(mesh);

if ndf<3
   plotnodes(xyz)
   plotmesh(mesh,xyz)
else
%% Plot nodes.
   set(gca,'DataAspectRatio',[1 1 1])
   node = cell(ngrid,1);
   for n=1:size(xyz,1), node{n}=int2str(n); end
   plot3(xyz(:,1), xyz(:,2), xyz(:,3), 'rx')
   text(xyz(:,1), xyz(:,2), xyz(:,3), node, 'Color', 'r')
   xlabel('X-axis')
   ylabel('Y-axis')
   zlabel('Z-axis')
   hold on

%% Plot elements
   for e=1:nelm
      line( xyz(mesh(e,[1:npe 1]),1), xyz(mesh(e,[1:npe 1]),2), xyz(mesh(e,[1:npe 1]),3) )
   end
   % Label the elements
   elem = cell(nelm,1);
   for e=1:nelm
      elem{e}=int2str(e);
   end
   xc = mean( reshape( xyz(mesh,1), size(mesh)), 2 );
   yc = mean( reshape( xyz(mesh,2), size(mesh)), 2 );
   zc = mean( reshape( xyz(mesh,3), size(mesh)), 2 );
   text(xc, yc, zc, elem, 'HorizontalAlignment', 'center')
   hold off
end
end