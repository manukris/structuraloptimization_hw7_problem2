function R = rhsload( Load, ndf, N )
% RHSLOAD  Right-Hand-Side Load vector generation for fea.m & FEAtruss.m
if ~iscell(Load.node),      Load.node=num2cell(Load.node(:),1); end
if ~iscell(Load.direction), Load.direction=num2cell(Load.direction(:),1); end
if ~iscell(Load.magnitude), Load.magnitude=num2cell(Load.magnitude(:),1); end
nlc=size(Load.node,2);
R = zeros(N,nlc);
for n=1:nlc
   % Determine degrees of freedom for Loading.
   ldof = ((Load.node{n}-1).*ndf + Load.direction{n})';
   %  Apply external loads.
   R(ldof,n) = Load.magnitude{n}(:);
end
end