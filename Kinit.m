function [K,N] = Kinit( ngrid, ndf, connect )
%KINIT Initialize stiffness matrix, K (full or sparse)
%
%--Input
%
%  ngrid.... Number of grid points (nodes)
%  ndf...... Nodal Degrees of Freedom per node
%  connect.. Nodal connectivity matrix for the mesh
%
%--Output
%
%  K........ Global Stiffness matrix
%  N........ Number of system degrees of freedom
%
%--Local variables
%
%  b........ semi-bandwidth of stiffness matrix
%  B........ Bandwidth of stiffness matrix
%  N........ number of global degrees of freedom

%  Allocate storage for the global stiffness matrix, based on bandwidth
b = (max(max(abs(diff(connect'))'))+1 )*ndf;
B = 2*b - 1;
N = ngrid*ndf;
if B < N
   K = spalloc(N,N,B*N-2*sum(1:b-1));
else
   K = zeros(N,N);
end
end
