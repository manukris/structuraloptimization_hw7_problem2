%% FEAtrussPost is a script to post-process (print) results from FEAtruss

[nelm,npe] = size( FEM.mesh );
[ngrid,ndf] = size( FEM.xyz );
nlc=size( FEM.R, 2);
FEM.R(abs(FEM.R)<100*eps)=0;

%% Print displacements for each load case
for n=1:nlc
disp(' ')
disp(['Load Condition ' num2str(n)])
switch ndf
   case 1
      disp('Node#           U          Rx')
      sfmt = '%5.0f %11.4g %11.4g\n';
   case 2
      disp('Node#           U           V          Rx          Ry')
      sfmt = '%5.0f %11.4g %11.4g %11.4g %11.4g\n';
   case 3
      disp('Node#           U           V          W          Rx          Ry          Rz')
      sfmt = '%5.0f %11.4f %11.4g %11.4g %11.4f %11.4g %11.4g\n';
end
disp(sprintf(sfmt,[1:ngrid; reshape(FEM.D(:,n),ndf,ngrid); reshape(FEM.R(:,n),ndf,ngrid)]))
end

%% Print stress for each truss element
disp(' ')
disp('Element# LoadCase#  Axial_Stress  Safety_Margin')
D = reshape(FEM.D,ndf,ngrid,nlc); MS=[];
for e=1:nelm
   for n=1:nlc
      if isfield(FEM.Material,'Allowable'), MS = 1 - FEM.stress(e,n) / FEM.Material.Allowable(e,n); end
      disp(sprintf('%8.0f %8.0f %14.4g  %13.4g',[e n FEM.stress(e,n) MS]))
   end
   if nlc>1, disp(' '), end
end