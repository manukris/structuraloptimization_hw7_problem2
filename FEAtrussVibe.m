%% FEAtrussVibe.m - PROCESSOR for Vibrations
% Script that must follow execution of FEAtruss
% FEM structure is assumed to exist

% Lumped mass matrix is diagonal
N = FEM.N;
M = spalloc(N,N,N);
ndf = size(FEM.xyz,2);
%  FOR each element generate and assemble mass.
for e=1:nelm
   m = truss_mass( FEM.Material.density, FEM.Area(e), FEM.L(e), ndf );
   p = locvec( FEM.mesh(e,:), ndf );
   M(p,p) = M(p,p) + m; 
%     M = assemble( m, M, FEM.mesh(e,:) );
end

%% Solve eigenvalue problem
if issparse(K)
   [eigvec_us,eigval_us] = eigs( K(u,u), M(u,u) );
   arealonly=real(eigval_us);
   [d,ind] = sort(diag(arealonly));
   eigvec=eigvec_us(:,ind);
   awholevec=diag(eigval_us);
   eigval=awholevec(ind);
else
   [eigvec_us,eigval_us] = eig( M(u,u)\K(u,u) );
   arealonly=real(eigval_us);
   [d,ind] = sort(diag(arealonly));
   eigvec=eigvec_us(:,ind);
   awholevec=diag(eigval_us);
   eigval=awholevec(ind);
end
if all(isreal(eigval))
   Frequencies = sort( sqrt( eigval ) );
   disp('Frequencies (rad/sec)'), disp(Frequencies)
   disp('Frequencies (Hz)'),      disp(Frequencies / (2*pi))
end